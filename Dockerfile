FROM debian:latest

ARG VERSION

RUN apt-get update && apt-get install xz-utils wget -y

RUN wget https://www.factorio.com/get-download/$VERSION/headless/linux64 -O /opt/factorio_headless.tar.xz

RUN cd /opt/ && tar -xJf factorio_headless.tar.xz

EXPOSE 34197

CMD ["/opt/factorio/bin/x64/factorio", "--start-server", "ryan-chris"]
