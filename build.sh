#!/bin/bash
if [ -z $1 ]; then
	echo "Version arguement required"
	exit 1
fi

VERSION=$1

docker build -t factorio:${VERSION} -t factorio:latest --build-arg VERSION=${VERSION} .
